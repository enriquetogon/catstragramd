This project was created with [Create React App](https://github.com/facebook/create-react-app).
# Set up

In order to run the project you need to install [Node.js ](https://nodejs.org/en/).

## Cloning repo

After that you need to clone the repository

```bash
git clone https://enriquetogon@bitbucket.org/enriquetogon/catstragramd.git
```

## Install the necessary modules

After cloning the repo, browse to it `cd catstragramd` and install the modules with `npm install`

## Create a localhost

Using `npm start` open [http://localhost:3000](http://localhost:3000) to see the app in the browser.
