import React from 'react';
import catstagramlogo from '../../assets/images/CatstagramLogo.svg'
import styles from './Logo.module.css';

const logo = (props) => (
    <div className={styles.Logo}>
        <img src={catstagramlogo} alt="Catstagram"/>
    </div>
);

export default logo;