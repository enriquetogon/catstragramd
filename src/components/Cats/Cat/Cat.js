import React from 'react';

import styles from './Cat.module.css';

const cat = (props) => {
    const columnMod = {
        padding: '2px'
    }
    let catImage = {
        backgroundImage: 'url(' + props.image + ')',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover'
    }
    return(
        <div className="col-lg-3 col-md-4 col-sm-6 col-xsm-12" style={columnMod}>
            <div className={styles.CatCard} style={catImage}  onClick={() => props.viewCat(props.cat)}>
            </div>
        </div>
    );
};

export default cat;