import React from 'react';

import Cat from './Cat/Cat';

const cats = (props) => {
    const cats = props.cats.map((cat) => {
        return <Cat image={cat.image} key={cat.catID} cat={cat}  viewCat={props.viewCat}/>
    });
    return(
        <div className="container">
            <div className="row">
                {cats}
            </div>
        </div>
    );
};

export default cats;