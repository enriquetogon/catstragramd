import React from 'react';
import styles from './CatLoader.module.scss';

const catLoader = (props) => {
    return(
        <div className={styles.buttonContainer}>
            <span className={styles.button} onClick={props.loadCats}>
                Mostrar más
            </span>
        </div>
    );
};

export default catLoader;