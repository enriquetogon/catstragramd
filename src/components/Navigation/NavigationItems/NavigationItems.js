import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './NavigationItems.module.css';

const navigationItems = (props) => (
    <ul className={styles.NavigationItems}>
        <li ><NavLink to="/" exact className={styles.NavButtons} activeClassName={styles.Active}><i className="material-icons">apps</i></NavLink></li>
        <li ><NavLink to="/Favourites" className={styles.NavButtons} activeClassName={styles.Active} ><i className="material-icons">favorite</i></NavLink></li>
    </ul>
);

export default navigationItems;