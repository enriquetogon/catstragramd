import React from 'react';

import styles from './Toolbar.module.css';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import SearchField from '../../UI/SearchField/SearchField'

const toolbar = (props) => {
    return(
        <header className={styles.Toolbar}>
        <Logo/>
           <SearchField/>
            <nav>
                <NavigationItems/>
            </nav>
        </header>
    );
    
};

export default toolbar;