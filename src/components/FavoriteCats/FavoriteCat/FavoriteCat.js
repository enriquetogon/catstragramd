import React from 'react';
import styles from './FavoriteCat.module.css';

const cat = (props) => {
    const columnMod = {
        padding: '2px'
    }
    let catImage = {
        backgroundImage: 'url(' + props.image + ')',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover'
    }
    return(
        <div className="col-lg-6 col-sm-12" style={columnMod}>
            <div className={styles.CatCard} style={catImage}  onClick={() => props.viewCat(props.cat)}>
            </div>
        </div>
    );
};

export default cat;