import React from 'react';

import FavoriteCat from './FavoriteCat/FavoriteCat';

const favoriteCats = (props) => {
    const favoriteCats = props.cats.map((cat) => {
        return <FavoriteCat  image={cat.image} key={cat.catID} cat={cat}/>
    });
    return(
        <div className="container">
            <h1>Favoritos</h1>
            <div className="row">
                {favoriteCats}
            </div>
        </div>
    );
};

export default favoriteCats;