import React from 'react';
import styles from './ImageView.module.css';
import Aux from '../../../hoc/Auxiliary/Auxiliary';
import Backdrop from '../Backdrop/Backdrop';

const imageView = (props) => {
    const anim = {
        transform: props.show ? 'scale(1)' : 'scale(0)',
        opacity: props.show ? '1' : '0'
    }
    return(
        <Aux>
            <Backdrop show={props.show} clicked={props.imageViewClosed}/>
            <img alt="Cat" src={props.selectedCat.image} className={styles.CatImage} style={anim} onDoubleClick={() => props.addedToFav(props.selectedCat.catID, props.userID)}/>
        </Aux>
    );
};

export default imageView;