import React, { Component } from 'react';
import { connect }from 'react-redux';

import styles from './SearchField.module.css';
import * as actionTypes from '../../../store/actions';

class SearchField extends Component{
    state = {
        searchText: ''
    };

    inputChanged = (text) => {
        this.setState({searchText: text.target.value});
        this.props.searchCats(text.target.value);
    }

    render(){
        return(
            <div className={styles.InputWrapper}>
                <input className={styles.InputField} onChange={(event) => this.inputChanged(event)} value={this.state.searchText} placeholder="Buscar por raza..."></input>
                <i className={"material-icons " + styles.SearchIcon}>search</i>
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        cats: state.cats
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        searchCats: (text) => dispatch(actionTypes.searchCatsByName(text)),
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(SearchField);