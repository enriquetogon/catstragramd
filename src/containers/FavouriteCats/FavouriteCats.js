import React, { Component } from 'react';
import { connect }from 'react-redux';

import FavoriteCats from '../../components/FavoriteCats/FavoriteCats';
import * as actionTypes from '../../store/actions';
//import axios from 'axios';

//import Aux from '../../hoc/Auxiliary/Auxiliary';

class FavouriteCats extends Component {
    componentDidMount() {
        if(this.props.favourites.length === 0){
            this.props.initFavourites(this.props.userID);
        }
    }
    
    render(){
        return(
            <FavoriteCats cats={this.props.favourites}/>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        cats: state.cats,
        favourites: state.favourites,
        userID: state.userID
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        initFavourites: (id) => dispatch(actionTypes.initFavourites(id)),
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(FavouriteCats);
