import React, {Component} from 'react';
import { connect }from 'react-redux';

import Aux from '../../hoc/Auxiliary/Auxiliary';
import Cats from '../../components/Cats/Cats';
import CatLoader from '../../components/Cats/CatLoader/CatLoader';
import ImageView from '../../components/UI/ImageView/ImageView';
import * as actionTypes from '../../store/actions';

class CatsGrid extends Component{
    state = {
        selectedCat: {},
        catBeingViewed: false,
        searchText: ''
    }

    viewCatHandler = (cat) =>{
        const selectedCat = cat;
        const catBeingViewed = true;
        this.setState({selectedCat: selectedCat, catBeingViewed: catBeingViewed})
    };

    closeViewHandler = () => {
        const catBeingViewed = false;
        this.setState({catBeingViewed: catBeingViewed});
    };

    componentDidMount() {
        if(this.props.cats.length === 0){
            this.props.initCats();
        }
    }

    render(){
        return(
            <Aux>
                <ImageView show={this.state.catBeingViewed} selectedCat={this.state.selectedCat} userID={this.props.userID} imageViewClosed={this.closeViewHandler} addedToFav={this.props.addFavourite}/>
                <Cats cats={this.props.cats} viewCat={this.viewCatHandler}/>
                <CatLoader loadCats={this.props.loadMoreCats}/>
            </Aux>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        cats: state.cats,
        favourites: state.favourites,
        userID: state.userID
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        initCats: () => dispatch(actionTypes.initCats()),
        loadMoreCats: () => dispatch(actionTypes.loadMoreCats()),
        addFavourite: (id,userID) => dispatch(actionTypes.addFavourite(id,userID))
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(CatsGrid);