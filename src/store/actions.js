import axios from 'axios';

export const SET_CATS = 'SET_CATS';
export const SHOW_MORE_CATS = 'SHOW_MORE_CATS';
export const SET_FAVOURITES = 'SET_FAVOURITES';
export const ADD_FAVOURITE = 'ADD_FAVOURITE';

export const setCats = (cats)=>{
    return {
        type: SET_CATS,
        cats: cats
    };
};

export const showMoreCats = (newCats) => {
    return {
        type: SHOW_MORE_CATS,
        cats: newCats
    };
};

export const setFavourites = (cats)=>{
    return {
        type: SET_FAVOURITES,
        favourites: cats
    };
};

export const addFavourites = (cats)=>{
    return {
        type: ADD_FAVOURITE,
        favourites: cats
    };
};

export const initCats = () => {
    return dispatch => {
        axios.get('/images/search',{
            params: {
                'limit': 12
            }
        }).then(response => {
            //console.log(response);
            const dirtyKitties = response.data;
            let cleanKitties = [];
            for(let i = 0; i < dirtyKitties.length; i++){
                let dirtyKitty = dirtyKitties[i];
                cleanKitties.push({
                    image: dirtyKitty.url,
                    catID: dirtyKitty.id
                }); 
            }
            dispatch(setCats(cleanKitties));
        }).catch(error => {
            console.log(error);
        });
    };
};

export const initFavourites = (id) => {
    return dispatch => {
        axios.get('/favourites',{
            params: {
                'sub_id': id
            }
        }).then(response => {
            //console.log(response);
            const dirtyKitties = response.data;
            let cleanKitties = [];
            for(let i = 0; i < dirtyKitties.length; i++){
                let dirtyKitty = dirtyKitties[i];
                cleanKitties.push({
                    image: dirtyKitty.image.url,
                    catID: dirtyKitty.image.id,
                    date: dirtyKitty['created_at']
                }); 
            }
            dispatch(setFavourites(cleanKitties));
        }).catch(error => {
            console.log(error);
        });
    };
};

export const loadMoreCats = () => {
    return dispatch => {
        //const oldCats = this.props.cats;
        axios.get('/images/search',{
            params: {
                'limit': 8
            }
        }).then(response => {
            const dirtyKitties = response.data;
            let cleanKitties = [];
            for(let i = 0; i < dirtyKitties.length; i++){
                let dirtyKitty = dirtyKitties[i];
                cleanKitties.push({
                    image: dirtyKitty.url,
                    catID: dirtyKitty.id
                }); 
            }
            //const updatedCats = [...oldCats, ...cleanKitties];
            dispatch(showMoreCats(cleanKitties));
        }).catch(error => {
            console.log(error);
        });
    };
};

export const searchCatsByName = (text)=> {
    return dispatch => {
        //console.log(text);
        //let searchText = event.target.value;
        axios.get('/images/search',{
            params: {
                'limit': 24,
                'breed_id': text
            }
        }).then(response => {
            console.log(response);
            const dirtyKitties = response.data;
            let cleanKitties = [];
            for(let i = 0; i < dirtyKitties.length; i++){
                let dirtyKitty = dirtyKitties[i];
                cleanKitties.push({
                    image: dirtyKitty.url,
                    catID: dirtyKitty.id
                }); 
            }
            dispatch(setCats(cleanKitties));
        }).catch(error => {
            console.log(error);
        });
    };
}

export const addFavourite = (id,userID) => {
    return dispatch => {
        //const oldCats = this.props.cats;
        axios.post('/favourites',{
                "image_id": id,
                "sub_id": userID
        }).then(response => {
            //console.log(response);
            dispatch(initFavourites(userID));
        }).catch(error => {
            console.log(error);
        });
    };
};