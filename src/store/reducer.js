import * as actionTypes from './actions';

const intialState = {
    cats: [],
    favourites: [],
    userID: 'testUser_01'
};

const reducer = (state = intialState,action) => {
    switch(action.type){
        case actionTypes.SET_CATS:
        return {
            ...state,
            cats: action.cats
        }
        case actionTypes.SHOW_MORE_CATS:
        return{
            ...state,
            cats: [...state.cats, ...action.cats]
        }
        case actionTypes.SET_FAVOURITES:
        return{
            ...state,
            favourites: action.favourites
        }
        default:
        return state;
    }
    
};

export default reducer;