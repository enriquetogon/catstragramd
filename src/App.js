import React from 'react';
import { Route } from 'react-router-dom';

import Layout from './components/Layout/Layout';
import CatsGrid from './containers/CatsGrid/CatsGrid';
import FavouriteCats from './containers/FavouriteCats/FavouriteCats';

function App() {
  return (
    <div>
      <Layout>
        <Route path="/" exact component={CatsGrid}/>
        <Route path="/Favourites" component={FavouriteCats}/>
      </Layout>
    </div>
  );
}

export default App;
